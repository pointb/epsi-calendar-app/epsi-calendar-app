import * as Linking from "expo-linking";

export default {
  prefixes: [Linking.makeUrl("/")],
  config: {
    screens: {
      Root: {
        screens: {
          Calendar: "Calendar",
          Profile: "Profile",
        },
      },
      First: {
        screens: {
          First: "First",
        },
      },
      Download: {
        screens: {
          Download: "Download",
        },
      },
      Profile: {
        screens: {
          profile: "Profile",
        },
      },
    },
  },
};
