import React from "react";
import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  createStackNavigator,
  StackNavigationProp,
} from "@react-navigation/stack";
import SyncButton from "../components/SyncButton";

import Colors from "../constants/Colors";
import useColorScheme from "../hooks/useColorScheme";
import CalendarScreen from "../screens/CalendarScreen";
import ProfileScreen from "../screens/ProfileScreen";
import {
  BottomTabParamList,
  CalendarParamList,
  ProfileParamList,
  RootStackParamList,
} from "../types";

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();
  return (
    <BottomTab.Navigator
      initialRouteName="Calendrier"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}
    >
      <BottomTab.Screen
        name="Calendrier"
        component={CalendarNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons size={30} name="calendar" color={color} />
          ),
        }}
      />
      <BottomTab.Screen
        name="Profil"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons size={30} name="cog-outline" color={color} />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

const CalendarStack = createStackNavigator<CalendarParamList>();

type CalendarNavigatorProps = {
  navigation: StackNavigationProp<RootStackParamList, "Root">;
};

function CalendarNavigator({ navigation }: CalendarNavigatorProps) {
  return (
    <CalendarStack.Navigator>
      <CalendarStack.Screen
        name="CalendarScreen"
        component={CalendarScreen}
        options={{
          headerTitle: "Calendrier",
          headerRight: () => <SyncButton navigation={navigation} />,
        }}
      />
    </CalendarStack.Navigator>
  );
}

const ProfileStack = createStackNavigator<ProfileParamList>();

function ProfileNavigator() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{ headerTitle: "Profil" }}
      />
    </ProfileStack.Navigator>
  );
}
