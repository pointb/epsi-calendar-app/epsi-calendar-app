export interface Course {
  date: string;
  name: string;
  teacher: string;
  group: string;
  classroom: string;
  time: string;
  duration: number;
  color: string;
}

export type RootStackParamList = {
  Root: { screen: string };
  Download: undefined;
  First: { screen: string };
  Profile: undefined;
};

export type BottomTabParamList = {
  Calendrier: undefined;
  Profil: undefined;
};

export type CalendarParamList = {
  CalendarScreen: undefined;
};

export type ProfileParamList = {
  ProfileScreen: undefined;
};
