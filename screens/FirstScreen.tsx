import React, { useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import { Input, Item, Button, Toast } from "native-base";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { StackNavigationProp } from "@react-navigation/stack";
import {
  Text as DefaultText,
  StyleSheet,
  Keyboard,
  Platform,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
} from "react-native";

import { View, Text } from "../components/Themed";
import Separator from "../components/Separator";
import { RootStackParamList } from "../types";

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "First">;
};

export default function FirstScreen({ navigation }: Props) {
  const [username, setUsername] = useState<string>("");

  const isUsernameValid = () =>
    new RegExp("[a-z0-9]{1,}[.{1}][a-z0-9]{1,}").test(username);

  const storeUsername = async () => {
    try {
      await AsyncStorage.setItem("@username", username);
      navigation.navigate("Root", { screen: "Calendar" });
    } catch (e) {
      Toast.show({
        text: "Une erreur est survenue.",
        buttonText: "Ok",
        position: "bottom",
        type: "danger",
      });
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.inner}>
          <Text>
            <Ionicons size={150} name="calendar" />
          </Text>
          <Separator />
          <Item style={styles.input}>
            <Input
              placeholder="prenom.nom"
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={(value: string) => setUsername(value)}
            />
          </Item>
          <Button
            block
            style={styles.button}
            disabled={!isUsernameValid()}
            onPress={storeUsername}
          >
            <DefaultText style={styles.buttonText}>Se connecter</DefaultText>
          </Button>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inner: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    marginLeft: "10%",
    marginRight: "10%",
    marginBottom: 10,
  },
  button: {
    marginHorizontal: "10%",
  },
  buttonText: {
    color: "#fff",
    fontWeight: "bold",
  },
});
