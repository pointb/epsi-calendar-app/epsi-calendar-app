import React, { useEffect, useState } from "react";
import { Toast } from "native-base";
import { StackNavigationProp } from "@react-navigation/stack";
import { StyleSheet, ActivityIndicator } from "react-native";
import { io } from "socket.io-client";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { Text, View } from "../components/Themed";
import { RootStackParamList } from "../types";
import Separator from "../components/Separator";

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "Download">;
};

export default function DownloadScreen({ navigation }: Props) {
  const [date, setDate] = useState<string>("");
  const [isConnected, setConnected] = useState<boolean | null>(null);

  useEffect(() => {
    const socket = io("http://127.0.0.1:5000");
    socket.emit("download", getUsername());
    socket.on("connect", () => setConnected(true));
    socket.on("progress", (date: string) => setDate(date));
    socket.on("disconnect", () => console.log("diconnected"));
    socket.on("complete", (data: any) => {
      storeData(data);
      navigation.navigate("Root", { screen: "Calendar" });
    });
    return () => {
      socket.disconnect();
    };
  }, []);

  const getUsername = async () => {
    try {
      const username = await AsyncStorage.getItem("@username");
      if (username === null) {
        return navigation.navigate("First");
      }
      return username?.toString();
    } catch (e) {
      Toast.show({
        text: "Une erreur est survenue.",
        buttonText: "Ok",
        position: "bottom",
        type: "danger",
      });
    }
  };

  const storeData = async (value: any) => {
    console.log(value);
    try {
      const jsonValue = JSON.stringify(value);
      await AsyncStorage.setItem("@data", jsonValue);
      Toast.show({
        text: "Les cours ont bien été récupérés !",
        buttonText: "Ok",
        position: "bottom",
        type: "success",
      });
    } catch (e) {
      Toast.show({
        text: "Une erreur est survenue.",
        buttonText: "Ok",
        position: "bottom",
        type: "danger",
      });
    }
  };

  const getStatusText = (status: boolean | null) => {
    switch (status) {
      case null:
        return "Connexion avec le serveur...";
      case true:
        return "Connecté au serveur";
      case false:
        return "Déconnecté du serveur";

      default:
        return null;
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Réupération des cours</Text>
      <Separator />
      <Text>{getStatusText(isConnected)}</Text>
      <ActivityIndicator style={styles.activityIndicator} size="large" />
      <Text>{date}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  activityIndicator: {
    marginVertical: 20,
  },
});
