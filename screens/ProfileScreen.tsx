import React from "react";
import { StyleSheet, Button } from "react-native";
import { Toast } from "native-base";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { Text, View } from "../components/Themed";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../types";

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "Profile">;
};

export default function ProfileScreen({ navigation }: Props) {
  const logout = async () => {
    try {
      await AsyncStorage.removeItem("@username");
      navigation.navigate("First", { screen: "First" });
    } catch (error) {
      Toast.show({
        text: "Une erreur est survenue.",
        buttonText: "Ok",
        position: "bottom",
        type: "danger",
      });
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Profil</Text>
      <View
        style={styles.separator}
        lightColor="#eee"
        darkColor="rgba(255,255,255,0.1)"
      />
      <Button title="Déconnexion" onPress={logout} color="#ff0000" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
