import React, { useState, useEffect, ComponentProps } from "react";
import { StyleSheet, Text, View, Alert } from "react-native";
import { Toast } from "native-base";
import { Agenda, AgendaItemsMap, LocaleConfig } from "react-native-calendars";
import AsyncStorage from "@react-native-async-storage/async-storage";

import {
  monthNames,
  monthNamesShort,
  dayNames,
  dayNamesShort,
} from "../constants/Locales";
import { Course } from "../types";

LocaleConfig.locales["fr"] = {
  monthNames,
  monthNamesShort,
  dayNames,
  dayNamesShort,
};
LocaleConfig.defaultLocale = "fr";

const CalendarScreen = () => {
  const [items, setItems] = useState<AgendaItemsMap<Course>>({});

  useEffect(() => {
    getItems();
    Alert.alert(
      "Mettre à jour le calendrier",
      "Votre calendrier n'a pas été mis à jour depuis plus d'une semaine !",
      [
        { text: "OK" }
      ],
      { cancelable: false }
    );
  }, []);

  const getItems = async () => {
    try {
      const jsonValue = await AsyncStorage.getItem("@data");
      const items = jsonValue != null ? JSON.parse(jsonValue) : {};
      setItems(items);
    } catch (e) {
      Toast.show({
        text: "Une erreur est survenue.",
        buttonText: "Ok",
        position: "bottom",
        type: "danger",
      });
    }
  };

  const renderItem = (item: any) => {
    return (
      <View
        style={[
          styles.item,
          { height: item.duration * 50, backgroundColor: item.color ?? "gray" },
        ]}
      >
        <Text>{item.time}</Text>
        <Text>{item.name}</Text>
        <Text>{item.teacher}</Text>
        <Text>{item.classroom}</Text>
        <Text>{item.group}</Text>
      </View>
    );
  };

  const renderEmptyDate = () => {
    return (
      <View style={styles.emptyDate}>
        <Text>This is empty date!</Text>
      </View>
    );
  };

  const rowHasChanged = (r1: Course, r2: Course) => {
    return r1.name !== r2.name;
  };

  return (
    <Agenda
      items={items}
      renderItem={renderItem}
      renderEmptyData={renderEmptyDate}
      rowHasChanged={rowHasChanged}
      firstDay={1}
      renderEmptyDate={() => <View />}
    />
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: "white",
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
  },
});

export default CalendarScreen;
