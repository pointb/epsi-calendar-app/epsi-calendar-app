import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Text } from "./Themed";
import { Ionicons } from "@expo/vector-icons";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../types";

type Props = {
  navigation: StackNavigationProp<RootStackParamList, "Root">;
};

const SyncButton = ({ navigation }: Props) => {
  const navigateToDownloadScreen = () => {
    navigation.navigate("Download");
  };

  return (
    <TouchableOpacity onPress={navigateToDownloadScreen}>
      <Text style={styles.text}>
        <Ionicons size={25} name="reload-outline" />
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  text: { marginRight: 15 },
});

export default SyncButton;
